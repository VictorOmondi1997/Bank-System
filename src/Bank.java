import java.util.Scanner;

public class Bank {
    private final int idNo;
    private final String name;
    private final long accNo;
    private double accBal;

    static Scanner sc = new Scanner(System.in);
    public Bank(){
        System.out.println("Create a new account");

        System.out.println("Enter account holder Name: ");
        name = sc.nextLine();

        System.out.println("Enter ID Number: ");
        idNo = sc.nextInt();

        System.out.println("Enter Account Number: ");
        accNo = sc.nextLong();

        System.out.println("Enter Opening Amount: ");
        accBal = sc.nextDouble();

        if (accBal>=200){
           System.out.println("Account Opened Successfully");
        }else {
            System.out.println("Add "+ (200-accBal)+ "for the Account Opening.");
            double amt = sc.nextDouble();
            accBal=accBal +amt;
        }
    }
    public double checkBalance(){
        return accBal;
    }
    public void deposit(){
        double amt;
        System.out.println("Enter amount to deposit ");
        amt = sc.nextDouble();
        System.out.println("Initial balance was "+accBal);
        accBal = accBal + amt;
        System.out.println("Balance after deposit is "+accBal);
    }
    public void  withdraw(){
        System.out.println("Enter amount withdraw");
        double amount = sc.nextDouble();
        if((accBal-amount)<200){
            System.out.println("You have insufficient funds to perform operation");
        }else {
            accBal=accBal-amount;
            System.out.println("Balance after withdrawal is"+accBal);
        }
    }
    public static void main(String[] args){
        Bank acc = new Bank();
        System.out.println("Current Balance is " +acc.checkBalance());

        acc.deposit();
        acc.withdraw();
    }
}
